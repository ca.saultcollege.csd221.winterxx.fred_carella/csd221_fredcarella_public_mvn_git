/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package w20.lecture5;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import w20.lecture5.entities.Book;
import w20.lecture5.entities.DiscMagazine;
import w20.lecture5.entities.Magazine;

/**
 *
 * @author student
 */
public class App {

    void run() {
        EntityManagerFactory emf = null;
        EntityManager em = null;

        try {
            emf = Persistence.createEntityManagerFactory("W20_LECTURE5_PU");
            em = emf.createEntityManager();
            Logger.getLogger(lab5.Main.class.getName()).log(Level.INFO, "Entity Manager created ({0})", emf);
            em.getTransaction().begin();
            Book book = new Book();
            Magazine mag = new Magazine(20, "April 2020");
            
            DiscMagazine discMag=new DiscMagazine(10, "Mar 2020", true);

            book.setAuthor("Fred Carella");
            book.setTitle("Freds Book");

            mag.setOrderQty(20);
            mag.setTitle("Freds Magazine");

            discMag.setTitle("Disc Mag");
            em.persist(book);
            em.persist(mag);
            em.persist(discMag);
            em.getTransaction().commit();
        } catch (Exception e) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (emf != null) {
                emf.close();
            }
//            if(em!=null)
//                em.close();
        }
    }

}

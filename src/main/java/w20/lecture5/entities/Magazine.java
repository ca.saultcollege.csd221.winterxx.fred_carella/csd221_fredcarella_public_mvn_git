package w20.lecture5.entities;

import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * @author student
 */

@Entity
public class Magazine extends Publication {

    @Basic
    private int orderQty;

    @Basic
    private String currIssue;

    public Magazine() {
    }

    public Magazine(int orderQty, String currIssue) {
        this.orderQty = orderQty;
        this.currIssue = currIssue;
    }

    public int getOrderQty() {
        return orderQty;
    }

    public void setOrderQty(int orderQty) {
        this.orderQty = orderQty;
    }

    public String getCurrIssue() {
        return currIssue;
    }

    public void setCurrIssue(String currIssue) {
        this.currIssue = currIssue;
    }

    public void recvNewIssue(String currIssue) {
        this.currIssue = currIssue;
    }

    @Override
    public void sellCopy() {
        // To change body of generated methods, choose Tools | Templates.
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
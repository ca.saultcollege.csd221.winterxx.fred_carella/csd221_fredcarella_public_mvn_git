package w20.lecture5.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import w20.lecture5.SaleableItem;

/**
 * @author student
 */

@Entity
public class Ticket implements SaleableItem, Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @Basic
    private double price;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public void sellCopy() {
        System.out.println("**************************************");
        System.out.println(" TICKET VOUCHER ");
        System.out.println(toString());
        System.out.println("**************************************");
        System.out.println();
    }

}